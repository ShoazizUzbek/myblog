from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from blog.models import New
from blog.serializer.FIlterSerializer import FilterSerializer


class FilteredNews(APIView):

    permission_classes = (AllowAny,)

    def post(self,request):
        serializer = FilterSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        tags = validated_data.get("tags")
        added_at = validated_data.get("added_at")
        filter_set = {}
        if tags:
            filter_set = {
                "tags__in":tags
            }
        if added_at:
            filter_set.update({
                "added_at":added_at
            })
        news = New.objects.prefetch_related("news_name").\
            filter(**filter_set)
        data = []
        for new in news:
            news_body = {
                "pk":new.pk,
                "author":new.author.username,
                "added_at":new.added_at
            }
            news_name_list = new.news_name.all()
            new_name_data = []
            for news_name in news_name_list:
                new_name_data.append({
                    "language":news_name.language,
                    "title":news_name.title,
                    "description":news_name.description
                })
            news_body.update({"news_name":new_name_data})
            data.append(news_body)
        return Response(data)
