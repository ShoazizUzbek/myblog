# Appitogram.uz Backend


### Git Clone
```bash
git clone https://gitlab.com/ShoazizUzbek/myblog.git
```


### Install requirements
```bash
pip install -r requirements.txt
```

### Initial migration on development PC
```bash
python manage.py migrate --settings=KashApp.settings.local
python manage.py makemigrations --settings=KashApp.settings.local
```

### Run Development Server
```bash
python manage.py runserver --settings=KashApp.settings.local
```


